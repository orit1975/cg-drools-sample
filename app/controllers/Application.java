package controllers;

import drools.Message;
import drools.TransactionEventDrools;
import play.mvc.*;
import java.util.*;

import plugins.Drools;

import javax.inject.Inject;

public class Application extends Controller {

    @Inject
    Drools drools;

    public Result index() {
                 
        final TransactionEventDrools  transaction = new TransactionEventDrools();
        transaction.setTxnType(TransactionEventDrools.TXN_TYPE_AUTH);
        transaction.setmtid(new String ("1240"));
        transaction.setProductID("INTERNAL_BALANCE_PRODUCT_ID");
        ArrayList<String> AuthChannels = new ArrayList<>();
        drools.kieSession.insert(transaction);
        drools.kieSession.setGlobal("AuthChannels", AuthChannels);
        drools.kieSession.fireAllRules();
        for(int i=0;i<AuthChannels.size();i++){
            System.out.println(AuthChannels.get(i));
        } 
        System.out.println(AuthChannels.size());
        //AMIT- in our real implementation we will execute here in parralel all the list
        // and wait for it to response, and if any of them is not eligible, act accordingly
        return ok("rules are running.. check the console.");
    }

}
