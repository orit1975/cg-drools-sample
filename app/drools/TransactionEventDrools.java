package drools;

public class TransactionEventDrools {
	
    public static final String TXN_TYPE_AUTH   = "A";
    public static final String TXN_TYPE_AUTHREVERSAL = "D";

    private String txnType;
    private String productID;
    private String mtid;

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }
    public String getMtid() {
        return mtid;
    }
    public void setmtid(String mtid) {
        this.mtid= mtid;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }
    public String getProductID() {
        return productID;
    }
}