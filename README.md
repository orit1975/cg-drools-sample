Play + Drools/cg
-------------

Sample application that shows how to integrate a Play Framework (Java) application with Drools.

Code:

    app/controllers/Application.java   HTTP request handlers (in the CG implementation will need to be merged with transaction handler)
    app/plugins/Drools.java            A singleton that sets up the Drools Session
    app/drools/Message.java            A Value Object that is sent to the rules engine

    conf/routes                        Defines a / HTTP handler pointing to app.controllers.Application.index()
    conf/META-INF/kmodule.xml          Config file for Drools that sets up a knowledge base pointing to the drools package
    app/drools/TransactionEventDrools   contain the event details that are used by drools
    conf/drools/EHI.drl                is the events rules
    build.sbt                          The build configuration that specifies dependencies on Drools
    
Run Locally:

1. Get this repo (e.g. `git clone https://github.com/jamesward/play-drools.git`)
2. Run: `./activator ~run`
3. Try it: [http://localhost:9000](http://localhost:9000)
4. The console should show a message was received and processed:

        goodbye, world
        hello, world
inspired by https://github.com/jamesward/play-drools
